﻿* Seleccion 4 */
/* 1 */
SELECT DISTINCT nombre, edad FROM ciclista  LEFT JOIN etapa USING(dorsal) WHERE numetapa IS NULL;
/* 2 */
SELECT DISTINCT nombre, edad FROM ciclista  INNER JOIN puerto USING(dorsal);
/* 3  ciclistas que han ganado alguna puerto O etapa durante toda la vuelta ciclista */
SELECT DISTINCT nombre, edad FROM ciclista  INNER JOIN etapa USING(dorsal) UNION
SELECT DISTINCT nombre, edad FROM ciclista  INNER JOIN puerto USING(dorsal);
/* ciclistas que han ganado alguna etapa Y tambien  algun puerto durante toda la vuelta ciclista */
SELECT DISTINCT nombre, edad
FROM (ciclista INNER JOIN puerto USING(dorsal)) INNER JOIN etapa USING(dorsal);
/* 4 */
SELECT DISTINCT director
FROM equipo INNER JOIN ciclista USING(nomequipo) INNER JOIN etapa USING(dorsal);
/* 5 */
SELECT DISTINCT dorsal, nombre
FROM ciclista INNER JOIN lleva USING(dorsal);
/* 6 */
SELECT DISTINCT dorsal, nombre
FROM ciclista INNER JOIN lleva USING(dorsal) INNER JOIN maillot USING(código)
WHERE color='Amarillo';
/* 7 */
SELECT DISTINCT ciclista.dorsal
FROM ciclista INNER JOIN lleva USING(dorsal) INNER JOIN etapa USING(dorsal);
/* 8 */
SELECT DISTINCT numetapa
FROM etapa INNER JOIN puerto USING(numetapa);
/* 9 */
SELECT DISTINCT kms
FROM ciclista INNER JOIN etapa USING(dorsal) INNER JOIN puerto USING(numetapa)
WHERE nomequipo="banesto";
/* 10 */
SELECT COUNT(*) FROM ciclista  INNER JOIN puerto USING(dorsal)
 INNER JOIN etapa
 ON puerto.numetapa=etapa.numetapa AND ciclista.dorsal=etapa.dorsal;
/* 11 */
SELECT DISTINCT nompuerto
FROM ciclista INNER JOIN puerto USING(dorsal)
WHERE nomequipo="banesto";
/* 12 */
SELECT Count(*)
FROM ciclista INNER JOIN etapa USING(dorsal) INNER JOIN puerto USING(numetapa)
WHERE nomequipo='BANESTO' AND kms>200;